package com.example.tsangle;

public class MovingAverage {
    private final int size;
    private float total = 0.0f;
    private int index = 0;
    private float values[];

    public MovingAverage(int size) {
        this.size = size;
        values = new float[size];
        for (int i = 0; i < size; i++) {
            values[i] = 0.0f;
        }
    }

    public void addValue(float value) {
        total -= values[index];
        values[index] = value;
        total += value;

        if (++index == size) index = 0;
    }

    public float getAverage() {
        return total / size;
    }
}
