package com.example.tsangle;

import android.app.Activity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.WindowManager;
import android.view.Surface;


public class Rotation implements SensorEventListener {
    public interface Listener {
        void rotationChanged(float yaw, float pitch, float roll);
        void inclinationChanged(int inclination);
        void rollChanged(float roll);
    }

    private Listener listener;
    private final WindowManager windowManager;
    private final SensorManager sensorManager;
    private final Sensor rotationSensor;
    private final Sensor accelerometerSensor;
    private int lastAccuracy;
    private final float ALPHA;
    float [] filteredValues;

    public Rotation(Activity activity) {
        windowManager = activity.getWindow().getWindowManager();
        sensorManager = (SensorManager) activity.getSystemService(Activity.SENSOR_SERVICE);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        ALPHA = 0.15f;
        filteredValues = new float[]{0.0f, 0.0f, 0.0f};
    }

    public void startListening(Listener newListener) {
        if (listener != newListener) {
            listener = newListener;
            sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_GAME);
            sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    public void stopListening() {
        sensorManager.unregisterListener(this);
        listener = null;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (listener != null) {
            if (sensorEvent.sensor == rotationSensor) {
                //updateRotationManually(sensorEvent.values);
                updateRotation(sensorEvent.values);
            }
            if (sensorEvent.sensor == accelerometerSensor) {
                //updateRotationManually(sensorEvent.values);
                updateAcceleration(sensorEvent.values);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //implement
    }

    private void updateRotation(float[] rotationValues) {
        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationValues);

        //SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Y, rotationMatrix);
        SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_Y, rotationMatrix);

        float[] orientation = new float[3];
        SensorManager.getOrientation(rotationMatrix, orientation);

        float azimuth = (float) Math.toDegrees(orientation[0]);
        float pitch = (float) Math.toDegrees(orientation[1]);
        float roll = (float) Math.toDegrees(orientation[2]);

        if (listener != null) {
            listener.rotationChanged(azimuth, pitch, roll);
        }
    }

    private float[] lowPass( float[] input, float[] output ) {
        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }


    private void updateAcceleration(float[] accelerometerValues) {
        float[] values = accelerometerValues;

        filteredValues = lowPass(values, filteredValues);

        float x = filteredValues[0];
        float y = filteredValues[1];
        float z = filteredValues[2];

        float vectorLen =(float) Math.sqrt(x * x + y * y + z * z);

        z = z / vectorLen;

        int inclination = (int) Math.round(Math.toDegrees(Math.acos(z)));

        if (listener != null) {
            listener.inclinationChanged(inclination);
        }

        float roll = (float)Math.toDegrees(Math.atan2(x, y));
        if (listener != null) {
            listener.rollChanged(roll);
        }
    }

}
