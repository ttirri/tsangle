package com.example.tsangle;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.pm.ActivityInfo;

public class MainActivity extends AppCompatActivity implements Rotation.Listener {

    private AngleView angleView;
    private Rotation rotation;

    @Override
    public void rotationChanged(float azimuth, float pitch, float roll) {
        angleView.setAngle(azimuth, pitch, roll);
    }

    @Override
    public void inclinationChanged(int inclination) {
        angleView.setInclination(inclination);
    }

    @Override
    public void rollChanged(float roll) {
        angleView.setRoll(roll);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        rotation = new Rotation(this);
        angleView = findViewById(R.id.angle_indicator);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        rotation.startListening(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        rotation.stopListening();
    }
}
