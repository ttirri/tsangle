package com.example.tsangle;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.Path;

import android.util.AttributeSet;
import android.view.View;

import android.os.Build;

import androidx.annotation.Nullable;

public class AngleView extends View {
    private final PorterDuffXfermode mXfermode;
    private final Paint mBitmapPaint;
    private final Typeface mTf;

    private int mWidth;
    private int mHeight;
    private float mAzimuth;
    private float mOldAzimuth = Float.NaN;
    private float mZeroAzimuth;
    private float mRoll;
    private float mZeroRoll;
    private float mInclination;
    private boolean skipFirst = true;

    private Bitmap mStaticBitmap;
    private Canvas mStaticCanvas;
    private Bitmap mDynamicBitmap;
    private Canvas mDynamicCanvas;
    private Context mContext;

    public AngleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER);
        mBitmapPaint = new Paint();
        mBitmapPaint.setFilterBitmap(false);

        mContext = context;

        mTf = Typeface.createFromAsset(context.getAssets(),"fonts/D-DIN.ttf");
    }

    private void drawProtractor(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.STROKE);

        float radius = canvas.getWidth() / 2.0f;
        canvas.drawCircle(canvas.getWidth() / 2.0f, canvas.getHeight() / 2.0f, radius, p);

        radius = 70.0f;
        canvas.drawCircle(canvas.getWidth() / 2.0f, canvas.getHeight() / 2.0f, radius, p);
    }

    private void drawTicks(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(0.0f);
        p.setStyle(Paint.Style.STROKE);
        p.setDither(true);

        int[] tickAngles = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180};

        float tickLen = 20.0f;
        float width = canvas.getWidth();
        float height = canvas.getHeight();
        float radius = width / 2.0f - tickLen;

        for (int tickAngle : tickAngles) {
            double angle = (Math.PI * tickAngle) / 180.0f;
            float startX = width / 2.0f + (float)Math.cos(angle) * radius;
            float startY = height / 2.0f + (float)Math.sin(angle) * radius;
            float stopX = width / 2.0f + (float)Math.cos(angle) * (radius + tickLen);
            float stopY = height / 2.0f + (float) Math.sin(angle) * (radius + tickLen);
            canvas.drawLine(startX, startY, stopX, stopY, p);

            startX = width / 2.0f - (float)Math.cos(angle) * radius;
            startY = height / 2.0f - (float)Math.sin(angle) * radius;
            stopX = width / 2.0f - (float)Math.cos(angle) * (radius + tickLen);
            stopY = height / 2.0f - (float) Math.sin(angle) * (radius + tickLen);
            canvas.drawLine(startX, startY, stopX, stopY, p);
        }
    }

    private void drawTickLabels(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(1.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setDither(true);
        p.setTypeface(mTf);
        p.setTextSize(25.0f);

        int[] tickAngles = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180};

        float width = canvas.getWidth();
        float height = canvas.getHeight();
        float radius = width / 2.0f - 45.0f;

        for (int tickAngle : tickAngles) {
            double angle = (Math.PI * tickAngle) / 180.0f + Math.PI / 2.0f;
            float centerX = width / 2.0f + (float)Math.cos(angle) * radius;
            float centerY = height / 2.0f + (float)Math.sin(angle) * radius;

            Rect textRect = new Rect();
            String text = String.valueOf(tickAngle);
            p.getTextBounds(text, 0, text.length(), textRect);

            canvas.save();
            canvas.rotate(tickAngle + 180.0f, centerX, centerY);
            canvas.drawText(text, centerX - textRect.width() / 2.0f, centerY, p);
            canvas.restore();

            if (tickAngle == 0 || tickAngle == 180)
                continue;

            centerX = width / 2.0f - (float)Math.cos(angle) * radius;
            centerY = height / 2.0f + (float)Math.sin(angle) * radius;

            canvas.save();
            canvas.rotate(180.0f - tickAngle, centerX, centerY);
            canvas.drawText(text, centerX - textRect.width() / 2.0f, centerY, p);
            canvas.restore();
        }
    }

    private float getAngleValue() {
        if (mInclination < 40) {
            return mAzimuth - mZeroAzimuth;
        }
        if (mInclination > 60) {
            return mRoll - mZeroRoll;
        }
        return 0.0f;
    }

    private void drawAngleText(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.RED);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setTypeface(mTf);
        p.setTextSize(60.0f);

        canvas.drawColor(Color.TRANSPARENT,PorterDuff.Mode.CLEAR);
        float width = canvas.getWidth();
        float height = canvas.getHeight();

        float rotationAngle = getAngleValue();
        String text = String.valueOf(Math.abs((int)(rotationAngle)));

        float x = width / 2.0f;
        float y = height / 2.0f;

        //draw angle text
        Rect textRect = new Rect();
        p.getTextBounds(text, 0, text.length(), textRect);
        x = x - textRect.width() / 2.0f;
        y = y + textRect.height() / 2.0f;

        canvas.drawText(text, x, y, p);
    }

    private void drawPointer(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        int angleColor = Color.parseColor("#F9423A");
        p.setColor(angleColor);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);

        float rotationAngle = getAngleValue();
        float width = canvas.getWidth();
        float height = canvas.getHeight();

        double angle = (Math.PI * (rotationAngle + 90.0f)) / 180.0f;
        float stopRadius = width / 2.0f - 20.0f;
        float startRadius = 70.0f;
        float arrorStartWidth = 8.0f;
        double arrowStartAngle = angle + Math.PI / 2.0f;

        float startX = width / 2.0f + (float)Math.cos(angle) * startRadius;
        float startY = height / 2.0f + (float)Math.sin(angle) * startRadius;

        float startX1 = startX + (float)Math.cos(arrowStartAngle) * arrorStartWidth;
        float startY1 = startY + (float)Math.sin(arrowStartAngle) * arrorStartWidth;
        float startX2 = startX - (float)Math.cos(arrowStartAngle) * arrorStartWidth;
        float startY2 = startY - (float)Math.sin(arrowStartAngle) * arrorStartWidth;


        float stopX = width / 2.0f + (float)Math.cos(angle) * stopRadius;
        float stopY = height / 2.0f + (float)Math.sin(angle) * stopRadius;

        Path arrowPath = new Path();
        arrowPath.setFillType(Path.FillType.EVEN_ODD);
        arrowPath.moveTo(startX1, startY1);
        arrowPath.lineTo(stopX, stopY);
        arrowPath.lineTo(startX2, startY2);
        arrowPath.lineTo(startX1, startY1);
        arrowPath.close();

        canvas.drawPath(arrowPath, p);
    }

    private void drawDynamic(Canvas canvas) {

/*
        text = String.valueOf((int)mPitch);
        canvas.drawText(text, x, y + 50, p);

        text = String.valueOf((int)mInclination);
        canvas.drawText(text, x, y + 100, p);
*/
        drawAngleText(canvas);
        drawPointer(canvas);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = w;
        mHeight = h;

        if (mWidth != 0 && mHeight != 0 && mStaticBitmap == null) {
            mStaticBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mStaticCanvas = new Canvas(mStaticBitmap);
            int bkColor = Color.parseColor(mContext.getString(R.color.angle_background));
            mStaticCanvas.drawColor(bkColor);
            //,PorterDuff.Mode.CLEAR
            drawProtractor(mStaticCanvas);
            drawTicks(mStaticCanvas);
            drawTickLabels(mStaticCanvas);
        }

        if (mWidth != 0 && mHeight != 0 && mDynamicBitmap == null) {
            mDynamicBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mDynamicCanvas = new Canvas(mDynamicBitmap);
            mDynamicCanvas.drawColor(Color.TRANSPARENT,PorterDuff.Mode.CLEAR);
        }
    }

    private int saveLayer(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return canvas.saveLayer(0, 0, mWidth, mHeight, null);
        } else {
            //noinspection deprecation
            return canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int sc = saveLayer(canvas);

        canvas.drawBitmap(mStaticBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(mXfermode);

        canvas.drawBitmap(mDynamicBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(null);

        canvas.restoreToCount(sc);
    }

    void handleAzimuth(float azimuth) {
        if (Float.isNaN(mOldAzimuth)) {
            mAzimuth = 0.0f;
            mZeroAzimuth = 0.0f;
            mOldAzimuth = azimuth;
            return;
        }
        //handle wrap around -180 -> 180
        if (mOldAzimuth < -90 && azimuth > 0.0f) {
            float diff = 360.0f + mOldAzimuth - azimuth;
            if (diff > 0.0f)
                mAzimuth += diff;
        } else if (mOldAzimuth > 90 && azimuth < 0.0f) {
            float diff = 360.0f - mOldAzimuth + azimuth;
            if (diff > 0.0f)
                mAzimuth -= diff;
        } else {
            float diff = azimuth - mOldAzimuth;
            mAzimuth += diff;
        }
        mOldAzimuth = azimuth;
    }

    void handleRoll(float roll) {
        //no need for special handling
        mRoll = roll;
    }

    //     pitch = x-axis, axis in screen plane from side to side
    //    roll = y-axis (axis in screen level from bottom to top, not used)
    //    azimuth = z-axis, our value, axis perpendicular to screen, point upwards from middle of screen
    //    azimuth values are [-180, 180], going over 180 wraps to -180, going over -180 wraps to 180
    public void setAngle(float azimuth, float pitch, float roll) {
        //first sensor values seems to be zero
        if (skipFirst) {
            skipFirst = false;
            return;
        }
        handleAzimuth(azimuth);

        if (mDynamicCanvas != null) {
            drawDynamic(mDynamicCanvas);
            invalidate();
        }
    }

    public void setInclination(int inclination) {
        if (Float.isNaN(mInclination)) {
            mInclination = inclination;
        }
        if (mInclination > 40 && inclination < 40) {
            mZeroAzimuth = mAzimuth;
        }
        if (mInclination < 60 && inclination > 60) {
            mZeroRoll = mRoll;
        }
        mInclination = inclination;
    }

    public void setRoll(float roll) {
        handleRoll(roll);
    }
}
